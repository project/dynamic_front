<?php

namespace Drupal\dynamic_front\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Url;
use Drupal\dynamic_front\Routing\DynamicFrontRoutes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure dynamic front settings for this site.
 */
class DynamicFrontSettingsForm extends ConfigFormBase {

  /**
   * The module config name.
   */
  const CONFIG_NAME = 'dynamic_front.settings';

  /**
   * Constructs a DynamicFrontSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routerBuilder
   *   The router builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected RouteBuilderInterface $routerBuilder) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dynamic_front_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'module' => static::CONFIG_NAME,
      'site' => 'system.site',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config_names = $this->getEditableConfigNames();
    $form['site_frontpage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default front page'),
      '#default_value' => $this->config($config_names['site'])->get('page.front'),
      '#required' => TRUE,
      '#placeholder' => '/tasks',
    ];
    $urls = $this->config($config_names['module'])->get('urls');
    $form['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Available URLs'),
      '#default_value' => $urls ? implode(PHP_EOL, $urls) : '',
      '#description' => $this->t('Enter a list of URLs. The first one the user has access to will open.'),
      '#required' => TRUE,
      '#placeholder' => '/tasks/all' . PHP_EOL . '/tasks/own' . PHP_EOL . '/user',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate front page.
    $new_front = trim($form_state->getValue('site_frontpage'));
    $form_state->setValue('site_frontpage', $new_front);
    if (UrlHelper::isValid($new_front)) {
      try {
        $route_name = Url::fromUserInput($new_front)->getRouteName();
        if ($route_name != DynamicFrontRoutes::ROUTE_NAME) {
          $form_state->setErrorByName('site_frontpage', $this->t("The path '%path' is already routed by: %route", [
            '%path' => $new_front,
            '%route' => $route_name,
          ]));
        }
      }
      catch (\Throwable) {
        // Ignore.
      }
    }
    else {
      $form_state->setErrorByName('site_frontpage', $this->t("Either the path '%path' is invalid.", ['%path' => $new_front]));
    }

    // Validate URLs.
    $urls = array_filter(array_map('trim', explode(PHP_EOL, $form_state->getValue('urls'))));
    $form_state->set('urls', $urls);
    foreach ($urls as $url) {
      if ($is_valid = UrlHelper::isValid($url)) {
        try {
          Url::fromUserInput($url);
        }
        catch (\Throwable) {
          $is_valid = FALSE;
        }
      }
      if (!$is_valid) {
        $form_state->setErrorByName('site_frontpage', $this->t("Either the path '%path' is invalid.", ['%path' => $url]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_names = $this->getEditableConfigNames();
    $site_config = $this->config($config_names['site']);
    $old_front = $site_config->get('page.front');
    $new_front = $form_state->getValue('site_frontpage');
    if ($old_front != $new_front) {
      $site_config->set('page.front', $new_front)->save();
      $this->routerBuilder->rebuild();
    }

    $this->config($config_names['module'])
      ->set('urls', $form_state->get('urls'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
