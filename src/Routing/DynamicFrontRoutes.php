<?php

namespace Drupal\dynamic_front\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Url;
use Drupal\dynamic_front\Controller\DynamicFrontController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides route for dynamic front.
 */
class DynamicFrontRoutes implements ContainerInjectionInterface {

  /**
   * The dynamic front route name.
   */
  const ROUTE_NAME = 'dynamic_front.redirect';

  /**
   * Constructs a DynamicFrontRoutes object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger factory.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelFactory $loggerFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes(): array {
    // Skip if front page is already routed.
    $front = $this->configFactory->get('system.site')->get('page.front');
    try {
      if (Url::fromUserInput($front)->getRouteName() != static::ROUTE_NAME) {
        $this->loggerFactory->get('dynamic_front')->warning('Front page is already routed.');
        return [];
      }
    }
    catch (\Throwable) {
      // Ignore.
    }

    // Add dynamic front route.
    $routes[static::ROUTE_NAME] = new Route(
      $front,
      ['_controller' => DynamicFrontController::class . '::redirect'],
      ['_permission' => 'access dynamic_front'],
      ['no_cache' => TRUE],
    );
    return $routes;
  }

}
