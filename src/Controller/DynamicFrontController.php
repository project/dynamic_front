<?php

namespace Drupal\dynamic_front\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\dynamic_front\Form\DynamicFrontSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Returns response for dynamic front page.
 */
class DynamicFrontController implements ContainerInjectionInterface {

  /**
   * Constructs a DynamicFrontController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger factory.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelFactory $loggerFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Builds the redirect response.
   */
  public function redirect(): RedirectResponse {
    $urls = $this->configFactory->get(DynamicFrontSettingsForm::CONFIG_NAME)->get('urls');
    $logger = $this->loggerFactory->get('dynamic_front');
    foreach ($urls as $url) {
      try {
        if (Url::fromUserInput($url)->access()) {
          return new RedirectResponse($url);
        }
      }
      catch (\Throwable $e) {
        $context = Error::decodeException($e);
        $context['@url'] = $url;
        $logger->error('Invalid URL: @url. ' . Error::DEFAULT_ERROR_MESSAGE, $context);
      }
    }
    throw new AccessDeniedHttpException();
  }

}
