Dynamic front [deprecated]
===============

Provides the ability to configure a dynamic front page, opening the first one
the user has access to.

Instructions
============

1. Enable the Dynamic front module.

2. Go to settings at /admin/config/system/dynamic-front.

3. Fill out the appropriate form and save.

Note.
A dynamic front page requires the "View dynamic front" permission, which
you must grant to the authenticated user.

Deprecated
============
[Dynamic links](https://www.drupal.org/project/dynamic_links) Dynamic links completely replace "Dynamic front" and also add the ability to configure any number of dynamic links that redirect to the first available one.
